<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ClienteDAO.php";
class Cliente{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $conexion;
    private $clienteDAO;

    public function getIdCliente(){
        return $this -> idCliente;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getEstado(){
        return $this -> estado;
    }

    public function Cliente($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($this -> idCliente, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> estado);
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }

    public function registrar(){
        $this -> conexion -> abrir();
        $codigoActivacion = rand(1000,9999);
        $this -> conexion -> ejecutar($this -> clienteDAO -> registrar($codigoActivacion));
        $this -> conexion -> cerrar();
        //Andrea revisar
      $url = "index.php?pid" .
                   base64_encode("presentacion/cliente/activarCliente.php") . "&correo=" .
                    $this -> correo . "&codigoActivacion=" .
                    base64_encode($codigoActivacion);
        echo $url;
        $asunto = "[Tienda Fruver] Activación de Nuevo Usuario";
        $mensaje = "Active su cuenta con la URL " . $url;
        $encabezado = array(
            "From" => "correo1@gmail.com",
            "Cc" => "correo2@gmail.com",
            "Bcc" => "correo3@gmail.com"
        );
        mail($this -> correo, $asunto, $mensaje, $encabezado);
    }

    public function activarCliente($codigoActivacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> verificarCodigoActivacion($codigoActivacion));
        if ($this -> conexion -> numFilas() == 1){
            $this -> conexion -> ejecutar($this -> clienteDAO -> activar());
            $this -> conexion -> cerrar();
            return true;
        }else {
          $this -> conexion -> cerrar();
            $this -> conexion -> cerrar();
            return false;
        }
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idCliente = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];

    }

}

?>
