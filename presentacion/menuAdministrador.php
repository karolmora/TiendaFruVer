<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<nav class="navbar navbar-expand-md navbar-light bg-info">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Crear</a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPagina.php") ?>">Consultar Pagina</a><a
						class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoTodos.php") ?>">Consultar Todos</a><a
						class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">Buscar</a>
				</div></li>
			<li class="nav-item"><a class="nav-link" href="#">Link</a></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() ?> <?php echo $administrador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Editar
						Perfil</a>
						 <a
						class="dropdown-item" href="#">Cambiar Clave</a>
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>
