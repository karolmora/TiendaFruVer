<nav class="navbar navbar-expand-lg navbar-light bg-light">
<img src="img/principal.png">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Producto
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Frutas</a>
          <a class="dropdown-item" href="#">Verduras</a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>

    <form class="form-inline my-2 my-lg-0 ">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Registrate</button>
    </form>
    <form class="form-inline my-2 my-lg-0 ">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Inicia Sesión</button>
    </form>
  </div>
</nav>

<div class="row">
	<!-- 	<div class="col-md-2 col-3 text-center">
			<a href="index.php"><img src="img/logo1.png" width="100px" ></a>
		</div> -->
		<div class="col-md-12 col-12 text-center">
			<h2><strong>TIENDA FRUVER</strong></h2>
		</div>
	</div>


<div class="container mt-3">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/4.png">
    </div>
    <div class="carousel-item">
      <img src="img/2.png">
    </div>
    <div class="carousel-item">
      <img src="img/3.jpg">
    </div>
    <div class="carousel-item">
      <img src="img/1.png">
    </div>
    <div class="carousel-item">
      <img src="img/5.jpg">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Antes</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>
</div>
<br><br>
<footer>
  <div class="container text-center">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h6 class="text-muted lead">Contactanos:</h6>
            <h6 class="text-muted">
            Carrera 8h No. 166-71 Bogotá D.C.<br>
            Teléfonos: 3115988953 – 3112641818.<br>
            </h6>
        </div>
        <div class="col-xs-12 col-md-6">
        <div class="pull-right">Encuentranos en las redes:</div>
              <div class="redes-footer">
                  <a href="https://www.facebook.com/"><img src="img/facebook.png"></a>
                  <a href="https://instagram.com/"><img src="img/instagram.png"></a>
                  <a href="https://twitter.com/"><img src="img/twitter.png"></a>
              </div>
        </div>
         <div class="col-md-12 col-12 text-center">
   <p class="text-muted small"> Fruver @2020. Todos los derechos reservados.</p>
   </div>
  </div>
</div>
</footer>



<div class="container mt-3">
	<div class="row">
		<div class="col-lg-9 col-md-8 col-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Tienda Fruver</h4>
				</div>
              	<div class="card-body">
              		<img src="img/portada.jpg" width="100%">
            	</div>
            </div>
		</div>
		<div class="col-lg-3 col-md-4 col-12 text-center">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Autenticacion</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="ingresar" type="submit" class="form-control btn btn-info">
    					</div>
    					<?php
    					if(isset($_GET["error"]) && $_GET["error"]==1){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
    					}else if(isset($_GET["error"]) && $_GET["error"]==2){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";
    					}else if(isset($_GET["error"]) && $_GET["error"]==3){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
    					}
    					?>
        			</form>
        			<p>Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php")?>">Registrate</a></p>
            	</div>
            </div>
		</div>
	</div>
</div>
