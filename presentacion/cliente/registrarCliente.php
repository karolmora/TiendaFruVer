<?php 
include "presentacion/encabezado.php";
$error = 0;
$registrado = false;
if(isset($_POST["registrar"])){
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $cliente = new Cliente("", "", "", $correo, $clave);
    if($cliente -> existeCorreo()){
        $error = 1;
    }else{
        $cliente -> registrar();
        $registrado = true;
    }
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Registro</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php") ?>" method="post">
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-info">
    					</div>	    					
        			</form>     
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						El cliente fue registrado exitosamente. Verifique el correo <?php echo $correo ?> para activar la cuenta.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>

