<?php 
include "presentacion/encabezado.php";
$correo = $_GET["correo"];
$codigoActivacion = base64_decode($_GET["codigoActivacion"]);
$cliente = new Cliente("", "", "", $correo);
$resultado = $cliente -> activarCliente($codigoActivacion);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Activar Cuenta</h4>
				</div>
              	<div class="card-body">        			    
        			<?php if($resultado){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						La cuenta con el correo <?php echo $correo ?> ha sido activada.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else { ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						La cuenta con el correo <?php echo $correo ?> NO ha sido activada. Revise su correo de activacion
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>					
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>

